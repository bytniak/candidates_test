
// This is a simple test to check ability to learn web3 basics
// author: Greg


const Web3 = require('web3'); //web3.js@1.0.0-beta.30
const provider = 'wss://ropsten.infura.io/ws'; //remote node provider
let web3 = new Web3(new Web3.providers.WebsocketProvider(provider));

if (web3.currentProvider != null) {
  console.log("Web3 version " + web3.version)
} else {
  console.log("Not connected to web3")
}

//This is the contract address, where the test was deployed
const contractAddress = "0xe2205e1e2299251f9e081daa0670b9d1b45c043d";

//This is contract abi (interface)
const abi = [
	{
		"constant": true,
		"inputs": [],
		"name": "getMessage",
		"outputs": [
			{
				"name": "",
				"type": "string"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": false,
				"name": "secondMessage",
				"type": "bytes32"
			}
		],
		"name": "LogSecondMessage",
		"type": "event"
	},
	{
		"inputs": [
			{
				"name": "_message",
				"type": "string"
			},
			{
				"name": "_secondMessage",
				"type": "bytes32"
			}
		],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "constructor"
	}
]

const ContractObject = new web3.eth.Contract(abi,contractAddress);

// You can ignore all above
// Below is the contract deployed on a blockchain, with which you have to interact.

/*
pragma solidity ^0.4.20;

contract Message{

    string messageStore;

    event LogSecondMessage(bytes32  secondMessage);

    function Message(string _message, bytes32 _secondMessage){
        messageStore = _message;
        LogSecondMessage(_secondMessage);
    }

    function getMessage() public constant returns(string){
        return messageStore;
    }

}
*/

// Provide solution to display (on index.html) 2 messages stored on a Blockchain (ethereum ropsen testnet)

// ** 1st a state variable "messageStore"
// ** 2nd a "secondMessage" log of the LogSecondMessage event

// All you need to do it to find right methods of the web3.js 1.0.0 library !
// Good luck !
// ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓





// ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑
// Thank you for you work!
