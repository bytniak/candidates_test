pragma solidity ^0.4.20;

contract Message{

    string messageStore;

    event LogSecondMessage(bytes32  secondMessage);

    function Message(string _message, bytes32 _secondMessage){
        messageStore = _message;
        LogSecondMessage(_secondMessage);
    }

    function getMessage() public constant returns(string){
        return messageStore;
    }

}
